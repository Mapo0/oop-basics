package com.Books;

public class Book {
    private String nameBook ;

    public Book(String nameBook) {
        this.nameBook = nameBook;
    }

    public Book() {
    }

    public String getNameBook() {
        return nameBook;
    }

    public void setNameBook(String nameBook) {
        this.nameBook = nameBook;
    }

    @Override
    public String toString() {
        return  nameBook + "\n";
    }
}
