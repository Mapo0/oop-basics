package com.company;

import com.Books.Book;
import com.ServiceLibrary.ServiceLibraryImpl;

import java.util.Arrays;

public class Main {


    public static void main(String[] args) {
        ServiceLibraryImpl serviceLibraryImpl= new ServiceLibraryImpl();
        System.out.print(Arrays.toString(serviceLibraryImpl.getLibrary().getBooks().toArray()));
        serviceLibraryImpl.addBook(new Book( "Война и мир") );
        System.out.print(Arrays.toString(serviceLibraryImpl.getLibrary().getBooks().toArray()));
        serviceLibraryImpl.deleteBook("Ведьмак");
        System.out.println(Arrays.toString(serviceLibraryImpl.getLibrary().getBooks().toArray()));
        System.out.print(serviceLibraryImpl.searchBook("Игра престолов"));

    }
}
