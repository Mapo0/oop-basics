package com.Library;

import com.Books.Book;

import java.util.ArrayList;
import java.util.Arrays;

public class Library {
    ArrayList<Book> books=new ArrayList<Book>(Arrays.asList(
            new Book("Игра престолов"),
            new Book("Гарри Поттер "),
            new Book("Москва петушки"),
            new Book("Ведьмак")
    ));

    public ArrayList<Book> getBooks() {
        return books;
    }
}
