package com.ServiceLibrary;

import com.Books.Book;

public interface ServiceLibrary {
    public void addBook(Book book);
    public void deleteBook(String nameBook);
    public Book searchBook(String nameBook);
}
