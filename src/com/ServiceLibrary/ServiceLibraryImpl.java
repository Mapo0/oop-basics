package com.ServiceLibrary;

import com.Books.Book;
import com.Library.Library;

public class ServiceLibraryImpl implements ServiceLibrary{
    Library library=new Library();
    @Override
    public void addBook(Book book) {
        library.getBooks().add(book);

    }

    @Override
    public void deleteBook(String nameBook) {
        for (Book book : library.getBooks()){
            if (book.getNameBook().equals(nameBook))
                library.getBooks().remove(book);
        }
    }

    @Override
    public Book searchBook(String nameBook) {
        for (Book book : library.getBooks()){
            if (book.getNameBook().equals(nameBook))
                return book;
        }
        return null;
    }

    public Library getLibrary() {
        return library;
    }
}
